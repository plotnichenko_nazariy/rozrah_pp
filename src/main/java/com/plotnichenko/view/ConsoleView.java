package com.plotnichenko.view;

import com.plotnichenko.FlowerType;
import com.plotnichenko.controller.Terminal;

import java.io.IOException;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ConsoleView {
private static Terminal terminal;
private static Scanner input;
    private Map<String, String> menu;
    private Map<String, Printable> methodsMenu;

static {
    input = new Scanner(System.in);
    terminal = new Terminal();
}
    public ConsoleView(){
        menu = new LinkedHashMap<>();
        menu.put("1", "  1 - print list of all goods");
        menu.put("2", "  2 - update list");
        menu.put("3", "  3 - add flowers");
        menu.put("4", "  4 - sort list");
        menu.put("Q", "  Q - exit");

        methodsMenu = new LinkedHashMap<>();
        methodsMenu.put("1", this::printFlowerList);
        methodsMenu.put("2", this::updateList);
        methodsMenu.put("3", this::addFlower);
        methodsMenu.put("4", this::sortList);
    }

    private void sortList() {
        terminal.sortFlowerList();
    }

    private void addFlower() {
        System.out.print("Please enter flower type and number of flowers, you want to add: ");
        String userString = null;
        try{
            userString = input.nextLine();
            if(!userString.matches("^[A-Z]+ \\d+")) throw new NumberFormatException();
            Pattern pattern;
            Matcher matcher;
            pattern = Pattern.compile("^[A-Z]+");
            matcher = pattern.matcher(userString);

            String stringtype;
            String number;
            String type;
            if(matcher.find()) {
                       type = (matcher.group());

            }
            else throw new UnavailableFlower();

            pattern = Pattern.compile("\\d+");
            matcher = pattern.matcher(userString);
            if(matcher.find()) {
                number = (matcher.group());
            }
            else throw new NumberFormatException();

            terminal.addFlowers(type, number);
        }

        catch (NumberFormatException e){
            System.out.println("Wrong input! Try again");
        } catch (UnavailableFlower unavailableFlower) {
            unavailableFlower.printStackTrace();
        }

    }

    public void printFlowerList(){

        System.out.println(terminal.getFlowerList().toString());
}

public void updateList(){

    try {
        terminal.updateFlowerListFromFile();
    } catch (IOException e) {
        e.printStackTrace();
    }
}
    public void show() {
        String keyMenu;
        do {
            outputMenu();
            System.out.println("Please, select menu point.");

            keyMenu = input.nextLine().toUpperCase();
            try {
                methodsMenu.get(keyMenu).print();
            } catch (Exception e) {
            }
        } while (!keyMenu.equals("Q"));
    }


    private void outputMenu() {
        System.out.println("\nMENU:");
        for (String str : menu.values()) {
            System.out.println(str);
        }
    }
}


