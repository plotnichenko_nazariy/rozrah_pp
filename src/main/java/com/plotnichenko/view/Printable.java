package com.plotnichenko.view;

@FunctionalInterface
public interface Printable {
    void print();
}
