package com.plotnichenko;

import com.plotnichenko.view.ConsoleView;

public class Application {
    public static void main(String[] args) {
        new ConsoleView().show();
    }
}
