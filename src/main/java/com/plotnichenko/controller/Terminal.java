package com.plotnichenko.controller;

import com.plotnichenko.Flower;
import com.plotnichenko.FlowerType;
import com.plotnichenko.model.FlowerShopImp;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.LinkedList;

public class Terminal {

    FlowerShopImp shop;

    public Terminal() {
        shop = new FlowerShopImp();
    }

    public LinkedList<Flower> getFlowerList() {
        return shop.getFlowerLinkedList();
    }



    public void sortFlowerList() {
        shop.sortFlower();
    }

    public void addFlowers(String group1, String group2) {
        int number = Integer.parseInt(group2);
        FlowerType type = FlowerType.valueOf(group1);
        for (int i = 0; i < number; i++) {
            shop.addFlower(new Flower(type));
        }
    }
    public void addFlowers(FlowerType ft, int number) {
        for (int i = 0; i < number; i++) {
            shop.addFlower(new Flower(ft));
        }
    }


    public void updateFlowerListFromFile() throws IOException {
        File file = new File("data.txt");
        BufferedReader reader = new BufferedReader(new FileReader(file));
        String line = null;


        String flowerType = null;
        while ((line = reader.readLine()) != null) {
            String[] lineArray = line.split(" ");
            flowerType = lineArray[0];
            int number = Integer.parseInt(lineArray[1]);
            addFlowers(FlowerType.valueOf(flowerType), number);
        }
        reader.close();

    }
}
