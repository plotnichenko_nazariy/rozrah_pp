package com.plotnichenko.model;

import com.plotnichenko.Flower;

import java.util.Comparator;
import java.util.LinkedList;

public class FlowerShopImp {
    private LinkedList<Flower> flowerLinkedList;

    public FlowerShopImp() {
        flowerLinkedList = new LinkedList<Flower>();
    }

    public LinkedList<Flower> getFlowerLinkedList() {
        return flowerLinkedList;
    }


    public void addFlower(Flower flower){
        flowerLinkedList.add(flower);
    }

    public void sortFlower(){
        flowerLinkedList.sort(Comparator.comparingInt(Flower::getPrice));
    }

}
